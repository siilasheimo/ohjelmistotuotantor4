
/*

Combine two correct words -type quiz. Words are retrieved 
from a text file, that needs to be in same directory. 
Word format in file:

word1;word2
word3;word4
ect.

*/

let storage = [];

/*
Input: Name of file to be opened (string).
Output:
    When failed: 'null'.
    When succeeded: swedish-finnish words
*/
function ScanTextFile(file_name="file1.txt") {

    var words = "No words found!";
    var reader = new XMLHttpRequest();

    reader.open("GET", file_name, false);

    reader.onreadystatechange = function() {
        
        if (reader.readyState === 4) {

            if (reader.status === 200 || reader.status === 0) {

                words = reader.responseText;
            }
        }
    }
    reader.send(null);
    
    if (reader.status != 200)
    {
        return null;
    }

    return words.split("\n");

}

/*
Input: Textfile in same directory.
Output: Grid in HTML-document has been initialized with 
    words from textfile.
*/
function InitWords() {

    var allWords = [];

    var textData = ScanTextFile();

    if (textData != null) {
        
        textData.forEach(element => {

            if (element.includes(";"))
            {

                let words = element.split(";");
                allWords.push(words);

            }
            
        });
    }
    else {
        console.log("Failed to open file!");
    }

    // Store words, and create HTML-elements

    var index = 1;

    let inputColumn = [];
    let leftColumn = [];
    let rightColumn = [];
    
    allWords.forEach(element => {

        // Input column
        var input = document.createElement("input");
        input.type = "number";
        input.min = 1;
        input.max = allWords.length;
        input.id = "INPUT" + String(index);
        inputColumn.push(input);

        // First word column
        var div = document.createElement("div");
        var text = document.createTextNode(element[0]);
        div.id = "LEFT" + String(index);
        div.className = "leftSide";
        div.appendChild(text);
        leftColumn.push(div);

        // Second word column
        div = document.createElement("div");
        text = document.createTextNode(element[1]);
        div.className = "rightSide";
        div.id = "RIGHT" + String(index);
        div.appendChild(text);
        rightColumn.push(div);

        index += 1;
    });

    // Randomize order of right column
    
    let rightColumnRandomized = [];
    
    while (rightColumn.length > 0)
    {
        let randomIndex = Math.floor(Math.random() * 10 % rightColumn.length);
        let removed = rightColumn.splice(randomIndex, 1);
        storage.push(
            {
                // Position number in randomized order
                key: rightColumnRandomized.length + 1,

                // If id = "RIGHT5" => idNumber = "5"
                idNumber: removed[0].id[5]
            }
        );
        rightColumnRandomized.push(removed[0]);
    }

    for (let index = 0; index < inputColumn.length; index++) {
        
        document.getElementById("grid").appendChild(
            inputColumn[index]
        );

        document.getElementById("grid").appendChild(
            leftColumn[index]
        );

        document.getElementById("grid").appendChild(
            document.createElement("div")
        );

        // Number label for right side words
        let numberDiv = document.createElement("div");
        numberDiv.className = "rightSideLabel";
        let numberText = document.createTextNode(String(index + 1));
        numberDiv.appendChild(numberText);
        document.getElementById("grid").appendChild(
            numberDiv
        )

        document.getElementById("grid").appendChild(
            rightColumnRandomized[index]
        );
        
    }

}


/*
Input: grid with answers from HTML-document
Output: count correct answers, and show count of 
    correct answers in HTML-document in label "resultLabel".
*/
function CheckWords() {

    // Mark all answers wrong by default, correct ones will be fixed after.
    document.getElementById("grid").childNodes.forEach(element => {

        if (element.id != undefined) {
            if (element.id.includes("LEFT")) {
                element.className = "wrongAnswer";
            }
        }

    });

    let checkButton = document.getElementById("checkButton");
    checkButton.setAttribute("disabled", true);

    var correct = 0;

    document.getElementById("grid").childNodes.forEach(element => {
        
        if (element.id != undefined) {

            if (element.id.includes("INPUT")) {

                var idNumber = element.id[5];
                var answer = element.value;
    
                storage.forEach(element => {

                    if (element.idNumber == idNumber 
                        && element.key == answer) {
    
                        correct += 1;

                        document.getElementById("LEFT"+ idNumber).className = "correctAnswer";
                    }

                });
            }
        }

        
        
    });
    
    let resultLabel = document.getElementById("resultLabel");
    resultLabel.textContent = String(correct) + "/" + String(storage.length);

}

/*
Input: Fetches grid from HTML-document
Output: Deletes all members of the grid, except for possible text,
    which is 0th member in childNodes.
*/
function DeleteWords() {

    let grid = document.getElementById("grid");

    while (grid.childNodes.length > 1)
    {
        grid.removeChild(grid.childNodes[1]);
    }

    storage = [];

}

/*
Input: grid from HTML-document
Output: old grid has been cleared, and replaced with 
    word from text file with randomized order.
*/
function Restart() {

    let checkButton = document.getElementById("checkButton");
    checkButton.removeAttribute("disabled");

    DeleteWords();

    let resultLabel = document.getElementById("resultLabel");
    resultLabel.textContent = "";

    InitWords();

}

InitWords();
